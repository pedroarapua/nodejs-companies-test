# test-agile-content

This project is about a log convertion and checking of the biggest subling number.

### Prerequisites

Install the dependencies

```
NodeJS 10.3+ (https://nodejs.org/en/download/)
```

### Installing

```sh
$ cd ${PROJECT}
$ npm install
```

## Running the tests

```sh
$ cd ${PROJECT}
$ npm test
```

### Using CLI ###

You could execute the application through a binary cli, you only need to execute the bellow command. 
Afterwards all commands you cloud replace `node cli` -> `test-agile-content`

```sh
$ cd ${PROJECT}
$ npm link
```

Show operations

```sh
$ node cli -h
```

Executing the biggest subling number

```sh
$ node cli bsn
```

Executing convert log from myCDN to Agora and save in output folder

```sh
$ node cli cla
```

## Built With

* [NodeJS](https://nodejs.org/dist/latest-v10.x/docs/api/) - The platform used
* [Jest](https://jestjs.io/docs/en/getting-started) - The framework to test all javaScript code
* [Flow](https://flow.org/en/docs/) - The framework to type inference

## Authors

* **Pedro Henrique F. Dias**