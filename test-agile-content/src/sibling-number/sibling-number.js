'use strict'
// @flow

/**
 * Class SiblingNumber
 */
export default class SiblingNumber {
  /**
   * Get biggest number
   */
  biggestNumber (value:number): number {
    let array: Array<string> = value.toString().split('')

    array = array.sort().reverse()
    const result: number = parseInt(array.join(''))

    return result > 100000000 ? -1 : result
  }
}
