#!/usr/bin/env node
'use strict'

const { prompt } = require('inquirer')
const program = require('commander')
const pkg = require('./package.json')

const Lib = require('.')
const defaultOutputFile = './output/myAgoraLog.log'
// changed url log because the original url isn't available
const defaultUrl = 'http://demo0651076.mockable.io/myCDNLog.txt' // http://logstorage.com/minhaCdn1.txt'

program
  .version(pkg.version)
  .description('test agile content')

program
  .command('biggest-sibling-number') // No need of specifying arguments here
  .alias('bsn')
  .description('Return the biggest sibling number')
  .action(() => {
    // Craft questions to present to users
    let questions = [
      {
        type: 'input',
        name: 'number',
        message: 'Enter a sibling number =>'
      }
    ]

    prompt(questions).then(answers => {
      const result = new Lib.SiblingNumber().biggestNumber(answers.number)
      console.log(`the biggest sibling number is => ${result}`)
    })
  })

program
  .command('convert-log-agora') // No need of specifying arguments here
  .alias('cla')
  .description('Return the log converted from myCDN to Agora and save in output folder')
  .action(async () => {
    // Craft questions to present to users
    let questions = [
      {
        type: 'input',
        name: 'url',
        message: `Enter a log myCDN url (default: "${defaultUrl}") =>`
      },
      {
        type: 'input',
        name: 'outputFile',
        message: `Enter a log myCDN url (default: "${defaultOutputFile}") =>`
      }
    ]

    let answers = await prompt(questions)
    let outputFile = answers.outputFile
    outputFile = !outputFile || outputFile === '' ? defaultOutputFile : outputFile
    let url = answers.url
    url = !url || url === '' ? defaultUrl : url

    console.log('Converting log to a file ...')
    console.log(`url => ${url}`)
    console.log(`outputFile => ${outputFile}`)
    try {
      const result = await new Lib.ConvertLog().convertMyCDNToAgora(url, outputFile)
      console.log(`### THE LOG CONVERTED IS ### \n\n${result}`)
    } catch (error) {
      console.error(`error => ${error}`)
    }
  })

program.parse(process.argv)
