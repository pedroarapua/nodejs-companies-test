'use strict'
import got from 'got'
import moment from 'moment'
import fs from 'fs'
// @flow

/**
 * Class ConvertLog
 */
export default class ConvertLog {
  /**
   * Create output log file
   */
  async createFile (outputStr: string, fullPath: string) {
    // delete file if exists and create file
    try {
      await this.deleteFileIfExists(fullPath)
      await fs.promises.writeFile(fullPath, outputStr)
    } catch (err) {
      throw err
    }
  }

  /**
   * Convert mycdnlog to agoralog
   */
  async deleteFileIfExists (fullPath: string) {
    let exists = false
    try {
      await fs.promises.access(fullPath, fs.constants.F_OK)
      exists = true
    } catch (error) {
      exists = false
    }

    // if exists file remove the same
    if (exists) {
      try {
        await fs.promises.unlink(fullPath)
      } catch (error) {
        throw error
      }
    }
  }

  /**
   * Convert mycdnlog to agoralog
   */
  async convertMyCDNToAgora (url: string, fullPath: string) {
    // request log
    let response: Object
    try {
      response = await got.get(url)
    } catch (err) {
      throw err
    }

    const responseBody: string = response.body

    let array: Array<string> = []
    // add header
    array.push('#Version: 1.0')
    array.push(`#Date: ${moment().format('DD/MM/YYYY HH:mm:ss')}`)
    // add fields
    array.push('#Fields: provider http-method status-code uri-path time-taken response-size cache-status')

    // split response in string array,
    // I had to add trimEnd to replace white space when get by url, apparentely response body is incorrect
    const lines: Array<string> = responseBody.trimRight().split('\n')

    lines.forEach((line, index) => {
      const fields: Array<string> = line.split('|')
      const httpData: Array<string> = fields[3].split(' ')

      const httpMethod: string = httpData[0].replace('"', '')
      const statusCode: string = fields[1]
      const uriPath: string = httpData[1]
      const timeTaken: string = parseInt(fields[4]).toString()
      const responseSize: string = fields[0]
      const cacheStatus: string = fields[2]

      let values: Array<string> = [
        '"MINHA CDN"',
        httpMethod,
        statusCode,
        uriPath,
        timeTaken,
        responseSize,
        cacheStatus
      ]

      array.push(values.join(' '))
    })

    // transform all itens in array to string
    let outputStr: string = array.join('\n')

    // create file in specific
    try {
      await this.createFile(outputStr, fullPath)
      outputStr = await fs.promises.readFile(fullPath, 'utf8')
    } catch (err) {
      throw err
    }

    return outputStr
  }
}
