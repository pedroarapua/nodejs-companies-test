'use strict'

import SiblingNumber from '../sibling-number'

const siblingNumber = new SiblingNumber()

describe('test SiblingNumber model', () => {
  describe('test biggestNumber function', () => {
    test('should be returned false - NaN (not a number)', () => {
      const number = 'abc'
      const result = siblingNumber.biggestNumber(number)

      expect(result).toBeFalsy()
    })

    test('should be returned -1', () => {
      const number = 100000001
      const result = siblingNumber.biggestNumber(number)

      expect(result).toBe(-1)
    })

    test('should be returned 321', () => {
      const number = 213
      const result = siblingNumber.biggestNumber(number)

      expect(result).toBe(321)
    })

    test('should be returned 532', () => {
      const number = 532
      const result = siblingNumber.biggestNumber(number)

      expect(result).toBe(532)
    })
  })
})
