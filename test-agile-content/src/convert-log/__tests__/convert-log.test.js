'use strict'

import got from 'got'
import fs from 'fs'
import path from 'path'
import ConvertLog from '../convert-log'

const convertLog = new ConvertLog()
const myCDNLog = fs.readFileSync(path.join(__dirname, '../__mocks__', 'myCDNLog.txt'), 'utf8')
const outputFile = path.join(__dirname, '../../../output/myAgoraLog.log')
// changed url log because the original url isn't available
const url = 'http://demo0651076.mockable.io/myCDNLog.txt' // http://logstorage.com/minhaCdn1.txt

jest.mock('moment', () => () => ({
  format: () => '10/12/2018 02:12:44'
}))

describe('test ConvertLog model', () => {
  describe('test convertMyCDNToAgora function', () => {
    test('should be returned RequestError of invalid url', async () => {
      got.get = jest.fn()
      got.get
        .mockRejectedValueOnce(new got.RequestError(new Error(), {}))

      await expect(convertLog.convertMyCDNToAgora('url'))
        .rejects
        .toThrowError(got.RequestError)
    })

    test('should be created a "Agora" log file', async () => {
      const myAgoraLog = fs.readFileSync(path.join(__dirname, '../__mocks__', 'myAgoraLog.txt'), 'utf8')
      got.get = jest.fn()
      got.get
        .mockReturnValueOnce({
          body: myCDNLog
        })

      fs.promises.access = jest.fn().mockRejectedValueOnce(new Error('Can\'t be access file'))
      fs.promises.unlink = jest.fn()
      fs.promises.writeFile = jest.fn()
      fs.promises.readFile = jest.fn().mockReturnValueOnce(myAgoraLog)

      const response = await convertLog.convertMyCDNToAgora(url, outputFile)

      expect(response).toEqual(myAgoraLog)
    })

    test('should be created a "Agora" log file, without ', async () => {
      got.get = jest.fn()
      got.get
        .mockReturnValueOnce({
          body: myCDNLog
        })

      const error = new Error('Can\'t be deleted')
      fs.promises.access = jest.fn()
      fs.promises.unlink = jest.fn().mockRejectedValueOnce(error)

      await expect(convertLog.convertMyCDNToAgora(url, outputFile))
        .rejects
        .toThrowError(error)
    })
  })
})
